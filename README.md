### Windows Media Player Native Subtitle Plugin
This has moved to [cpp projects - WMPSubtitlePlugin](https://github.com/atiq-cs/cpp/tree/dev/Win32/P21_WMPSubtitlePlugin) including source files.

#### Samples
WMPPluginDsp is archived in this repo which was most likely part of Windows 7 SDK's WMP SDK Sample. Please look for the dir "WMPPluginDsp" above.
WMP Samples are listed in: [Win Samples](https://github.com/atiq-cs/cpp/blob/dev/Win32/Samples.md).

**Additional Files**  
*x86 dir* contains solution and project files which are redundant. Solution and vcxproj files on root dir cover target specs for both x64 and x86.
/////////////////////////////////////////////////////////////////////////////
//
// CWmpplugin_dspPropPage.h : Declaration of CWmpplugin_dspPropPage
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//
/////////////////////////////////////////////////////////////////////////////

#ifndef __CWMPPLUGIN_DSPPROPPAGE_H_
#define __CWMPPLUGIN_DSPPROPPAGE_H_

#include "resource.h"
#include "wmpplugin_dsp.h"

// {B80FC511-EF09-454C-9EB9-A7D2961A5AD8}
DEFINE_GUID(CLSID_Wmpplugin_dspPropPage, 0xb80fc511, 0xef09, 0x454c, 0x9e, 0xb9, 0xa7, 0xd2, 0x96, 0x1a, 0x5a, 0xd8);

/////////////////////////////////////////////////////////////////////////////
// CWmpplugin_dspPropPage
class ATL_NO_VTABLE CWmpplugin_dspPropPage :
    public CComObjectRootEx<CComMultiThreadModel>,
    public CComCoClass<CWmpplugin_dspPropPage, &CLSID_Wmpplugin_dspPropPage>,
    public IPropertyPageImpl<CWmpplugin_dspPropPage>,
    public CDialogImpl<CWmpplugin_dspPropPage>
{
public:
            CWmpplugin_dspPropPage(); 
    virtual ~CWmpplugin_dspPropPage(); 
    

    enum {IDD = IDD_WMPPLUGIN_DSPPROPPAGE};
    

DECLARE_REGISTRY_RESOURCEID(IDR_WMPPLUGIN_DSPPROPPAGE)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CWmpplugin_dspPropPage) 
    COM_INTERFACE_ENTRY(IPropertyPage)
END_COM_MAP()

BEGIN_MSG_MAP(CWmpplugin_dspPropPage)
    CHAIN_MSG_MAP(IPropertyPageImpl<CWmpplugin_dspPropPage>)
    COMMAND_HANDLER(IDC_SCALEFACTOR, EN_CHANGE, OnChangeScale)
    MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
END_MSG_MAP()

    STDMETHOD(SetObjects)(ULONG nObjects, IUnknown** ppUnk);
    STDMETHOD(Apply)(void);

    LRESULT (OnChangeScale)(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
    LRESULT (OnInitDialog)(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

private:
    CComPtr<IWmpplugin_dsp> m_spWmpplugin_dsp;  // pointer to plug-in interface
};

#endif // __CWMPPLUGIN_DSPPROPPAGE_H_
